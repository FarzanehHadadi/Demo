// export interface IQuestionList {
//   questions: IQuestion[];
// }
export interface IResult {
  category: string;
  type: string;
  question: string;
  correct_answer: string;
  incorrect_answers: string[];
}
