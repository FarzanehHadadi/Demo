import React, { useRef, useState } from "react";
import {
  Button,
  Container,
  FormControl,
  InputBase,
  InputLabel,
  makeStyles,
  MenuItem,
  NativeSelect,
  Select,
  withStyles,
} from "@material-ui/core";
import theme from "../../styles/theme";

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3),
    },
  },

  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    minWidth: "100px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}))(InputBase);

const useStyles = makeStyles((theme) => ({
  margin: { margin: theme.spacing(1) },
}));
const QuizForm = () => {
  const classes = useStyles();
  const [type, typeSet] = useState<string>("sports");
  const [difficulty, difficultySet] = useState<string>("easy");
  const [count, countSet] = useState<string>("10");
  const handleChangeDifficulty = (event: any) => {
    difficultySet(event.target.value);
  };
  const handleChangeCount = (event: any) => {
    console.log(event.target.value);

    countSet(event.target.value);
  };
  const handleChangeType = (e: any) => {
    typeSet(e.target.value);
  };

  return (
    <Container>
      <div>
        <FormControl
          className={classes.margin}
          onSubmit={(e) => e.preventDefault()}
        >
          <InputLabel>Type</InputLabel>
          <Select
            id="type-select"
            value={type}
            onChange={handleChangeType}
            input={<BootstrapInput />}
          >
            <MenuItem value="sports">Sports</MenuItem>
            <MenuItem value="animals">Animals</MenuItem>
            <MenuItem value="history">History</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.margin}>
          <InputLabel id="demo-customized-select-label">Difficulty</InputLabel>
          <Select
            id="difficulty-select"
            value={difficulty}
            onChange={handleChangeDifficulty}
            input={<BootstrapInput />}
          >
            <MenuItem value="easy">Easy</MenuItem>
            <MenuItem value="medium">Medium</MenuItem>
            <MenuItem value="hard">Hard</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.margin}>
          <InputLabel htmlFor="demo-customized-select-native">Count</InputLabel>
          <Select
            id="count-select"
            value={count}
            onChange={handleChangeCount}
            input={<BootstrapInput />}
          >
            <MenuItem value="10">10</MenuItem>
            <MenuItem value="20">20</MenuItem>
            <MenuItem value="30">30</MenuItem>
          </Select>
        </FormControl>
      </div>

      <Button
        type="submit"
        color="primary"
        variant="contained"
        className={classes.margin}
      >
        Submit
      </Button>
    </Container>
  );
};

export default QuizForm;
