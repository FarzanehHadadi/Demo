import { Button, CircularProgress, IconButton } from "@material-ui/core";
import React from "react";
import { useState, useEffect, FC } from "react";
import { IResult } from "../constant/Types";
import QuizForm from "./QuizForm";
import SingleQuestion from "./SingleQuestion";
import SkipNextSharpIcon from "@material-ui/icons/SkipNextSharp";

const QuestionList: FC<IResult[]> = () => {
  const [loading, loadingSet] = useState(true);
  const [active, activeSet] = useState<number>(0);
  const [error, errorSet] = useState(false);
  const [quiz, quizSet] = useState([]);
  const [examCompleted, examCompletedSet] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    loadingSet(true);
    const res = await fetch(
      `https://opentdb.com/api.php?amount=10&category=22&difficulty=medium&type=multiple`
    );
    const data = await res.json();

    if (!data) {
      errorSet(true);
    } else {
      const results: IResult[] = data.results;
      console.log("results", results);

      quizSet(results);
      loadingSet(false);
    }
  };
  //   console.log(quiz);
  const nextQuestion = () => {
    if (active === quiz.length - 1) {
      examCompletedSet(true);
    } else {
      activeSet(active + 1);
    }
  };
  if (loading) {
    return (
      <div>
        <CircularProgress />
        <div>
          <h2>hello</h2>
        </div>
      </div>
    );
  }
  if (error) {
    return (
      <div>
        <h3>failed to load quiz</h3>
      </div>
    );
  }
  if (examCompleted) {
    return <QuizForm />;
  }
  return (
    <div>
      <h1>Questions</h1>
      {console.log("very bad quiz", quiz)}
      <SingleQuestion quiz={quiz[active]} loading={loading} />
      <div style={{ textAlign: "center" }}>
        <IconButton aria-label="next" onClick={nextQuestion}>
          <SkipNextSharpIcon style={{ fill: "green" }} />
        </IconButton>
      </div>
    </div>
  );
};
// export async function getStaticProps() {
//   let loading = true;
//   const res = await fetch(
//     `https://opentdb.com/api.php?amount=10&category=22&difficulty=medium&type=multiple`
//   );
//   const data = await res.json();
//   loading = false;

//   if (!data || loading) {
//     return {
//       notFound: true,
//     };
//   }
//   const results: IResult[] = data.results;
//   return {
//     props: { results }, // will be passed to the page component as props
//   };
// }
export default QuestionList;
