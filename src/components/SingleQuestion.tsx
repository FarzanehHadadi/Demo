import {
  Button,
  FormControlLabel,
  makeStyles,
  Radio,
  RadioGroup,
  Typography,
} from "@material-ui/core";
import React, { useState, useEffect, FC } from "react";
import { IResult } from "../constant/Types";
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(3),
  },
  button: {
    margin: theme.spacing(1, 1, 0, 0),
  },
}));
const SingleQuestion: FC<IResult> = ({ quiz }: IResult | any, loading) => {
  console.log("quiz", quiz);
  const classes = useStyles();
  const [value, setValue] = useState("");
  const [error, setError] = useState(false);
  const [helperText, setHelperText] = useState("Choose wisely");
  const [answers, answersSet] = useState<string[]>([]);
  const handleRadioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    setHelperText(" ");
    setError(false);
  };

  const createAnswers = () => {
    let tempAnswers = [...incorrect_answers];
    console.log("incorrect_answers in start", incorrect_answers);
    const randomIndex = Math.floor(Math.random() * 4);

    if (randomIndex === 3) {
      tempAnswers.push(correct_answer);
    } else {
      tempAnswers.push(answers[randomIndex]);
      tempAnswers[randomIndex] = correct_answer;
    }
    console.log("tempAnswers", tempAnswers);

    answersSet([...tempAnswers]);

    console.log("randomIndex", randomIndex);
  };
  useEffect(() => {
    console.log("from useEffect");

    createAnswers();
  }, [quiz]);
  const handleSubmit = (event) => {
    event.preventDefault();

    if (value === "best") {
      setHelperText("You got it!");
      setError(false);
    } else if (value === "worst") {
      setHelperText("Sorry, wrong answer!");
      setError(true);
    } else {
      setHelperText("Please select an option.");
      setError(true);
    }
  };
  if (loading) {
    return <div></div>;
  }
  const { correct_answer, incorrect_answers, question } = quiz;
  console.log(answers);
  return (
    <div>
      <Button onClick={createAnswers} variant="contained" color="secondary">
        create Random
      </Button>
      <Typography variant="button" display="block" gutterBottom>
        {question}
      </Typography>
      <RadioGroup
        aria-label="quiz"
        name="quiz"
        value={value}
        onChange={(e) => handleRadioChange(e)}
      >
        {answers.map((answer, index) => {
          return (
            <FormControlLabel
              key={index}
              value={answer}
              control={<Radio />}
              label={answer}
            />
          );
        })}
      </RadioGroup>
    </div>
  );
};

export default SingleQuestion;
